<?php

namespace whitecat636\extendedusuario;

use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

class Bootstrap implements BootstrapInterface {

    /** @inheritdoc */
    public function bootstrap($app) {

        
        if (!isset($app->get('i18n')->translations['extendedusuario*'])) {
            $app->get('i18n')->translations['extendedusuario*'] = [
                'class' => PhpMessageSource::className(),
                'basePath' => __DIR__ . '/messages',
                'sourceLanguage' => 'en-US'
            ];
        }
        
    }

}
